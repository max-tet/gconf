# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.8.0] - 2021-05-15

### Added

- Option to create independent GConf instances

## [0.7.1] - 2021-01-08

### Fixed

- Stupid import error

## [0.7.0] - 2021-01-08

### Added

- Support for config values override through environment variables

## [0.6.6] - 2020-05-18

### Fixed

- Use the "rules" syntax instead of "only" in gitlab-ci (https://docs.gitlab.com/ee/user/application_security/#transitioning-your-onlyexcept-syntax-to-rules)

## [0.6.5] - 2020-04-08

### Fixed

- Setting a default value with `get()` now supports falsey values (`False`, `None`, empty string)

## [0.6.4] - 2020-03-01

### Fixed

- Empty config files no longer cause an error and are ignored instead

## [0.6.3] - 2019-11-15

### Changed

- A deprecation warning has been removed

## [0.6.2] - 2019-04-27

### Changed

- A `FileNotFoundError` now contains the fully resolved file path(s) 

## [0.6.1] - 2019-04-13

### Changed

- Minor improvement in maintainability

## [0.6.0] - 2019-04-11

### Fixed

- (#8) `get()` on an item that is a dict or list now correctly returns the overlay of all loaded configs

## [0.5.5] - 2019-03-23

### Changed

- Changed YAML loading to safe mode

## [0.5.4] - 2019-03-14

### Changed

- Improved information in setup.py

## [0.5.2] - 2019-03-12

### Added

- Added CI/CD

## [0.5.1] - 2019-03-10

### Changed

- Attempting to load a non-existing config now raises a `FileNotFoundError`
- The package can now be istalles through PyPI

## [0.5.0] - 2018-10-18

### Added

- (#3) Added the `add()` method for loading a config from a python dict

## [0.4.0] - 2018-09-27

### Added

- (#7) Top-level `load_first()` method
- Boolean keyword-argument `required` to all loading methods

### Fixed

- (#6) Loading missing config files now raises the correct exception
